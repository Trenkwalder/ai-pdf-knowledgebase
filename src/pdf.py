import functools


@functools.cache  # to speed up tests
def read(file_path: str) -> dict:
    """
    This function reads a PDF and its content structured

    :param file_path: the path to the pdf
    :return: the content of the pdf
    """
    from PyPDF2 import PdfReader

    pdf = PdfReader(file_path)  # type: ignore
    return {
        "title": pdf.metadata.title,
        "content": "".join([page.extract_text() for page in pdf.pages]),
    }
