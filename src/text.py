from typing import List


def clean_text(text: str) -> str:  # type: ignore
    """
    A function that should clean a text
    (e.g., from unwanted word splits at the end of the line)
    TODO: This is a simple function that should be extended
          (e.g., to remove image or table artefacts in the text)

    :param text: text to be cleaned
    :return: cleaned text
    """
    import re

    no_new_lines_in_sentences = re.sub(r"([a-z]+)[-]{0,1}\n([a-z]+)", r"\1 \2", text)
    no_hyphens_in_words = re.sub(
        r"([a-z]+)-([a-z]+)", r"\1\2", no_new_lines_in_sentences
    )
    return no_hyphens_in_words


def split(text: str) -> List[str]:
    """
    This function splits a long text into chunks
    with ensures a maximum amount of tokens.
    TODO: This is a simple character splitter.
          At some point, splitting by sentences/paragraphs would be great.

    :param text: is the single block of text
    :return: a list of text chunks
    """
    from langchain.text_splitter import CharacterTextSplitter

    text_splitter = CharacterTextSplitter(
        separator="\n",
        chunk_size=1000,
        chunk_overlap=200,
    )
    return text_splitter.split_text(text)
