from pathlib import Path

import pytest

from src.pdf import read
from src.text import clean_text, split


@pytest.fixture(scope="module")
def loaded_sct_paper():
    file_path = Path(__file__).resolve().parent / "pdfs/sct_2016.pdf"

    data = read(str(file_path))
    yield data


@pytest.mark.parametrize(
    "expected_content",
    [
        "However, they usually do not guarantee that the implementation "
        "matches the speciﬁcation, because the system’s "
        "control code is typically generated manually.",
        "To test the scalability of the approach, 600 Kilobots were used "
        "in the group formation experiment.",
    ],
)
def test_sentences_should_not_contain_a_new_line(loaded_sct_paper, expected_content):
    data = loaded_sct_paper["content"]

    print(clean_text(data))

    assert expected_content in clean_text(data)


@pytest.mark.parametrize(
    "expected_content",
    [
        "The control software of swarm robotics systems is usually obtained "
        "through ad hoc devel-\nopment, without relying on software "
        "engineering methods. The ad hoc development, whichis mainly used in "
        "academic environments, hinders the transition of swarm robotics "
        "systemsto real-world applications. The source code resulting from ad "
        "hoc development is difﬁcult tobe maintained, analysed, "
        "or veriﬁed.\nFormal methods help in addressing these problems (for "
        "example, see Knight et al. (1997 )).\nThey require a systematic "
        "formalisation of the solutions. Such formalisation can be "
        "subjectedto analysis tools, for example, to verify that certain "
        "properties are met. They also serve as adocumentation of the "
        "system.\nHowever, even when formal methods were used, it was not "
        "guaranteed that the ﬁnal\nsource code would accurately represent the "
        "speciﬁcations. This is because the source codewas obtained in a "
        "manual process as automatic code generation has not been supported "
        "yet.",
        "Fierro et al. (2001 ) use HST to control the formation of a group of "
        "three robots moving\nalong a given trajectory. Here the "
        "discretisation is at the controller level: the robot has "
        "multiplecontinuous motion controllers. Its sensory input—which other "
        "robots are perceived—is usedto select the controller to be executed. "
        "In McNew and Klavins (2006 );McNew et al. (\n2007 ),\nHST is used "
        "for the problem of organising the robots into subgroups while "
        "maintaining the\n123Swarm Intell (2016) 10:65–97 69\noverall "
        "connectivity. The formal properties are guaranteed by the use of "
        "embedded graph\ngrammars.\nAs shown in this paper, SCT allows for "
        "automatic code generation for a swarm of robots.",
    ],
)
def test_text_chunks_can_be_split(loaded_sct_paper, expected_content):
    data = loaded_sct_paper["content"]

    chunks = split(data)
    print(chunks)

    assert len(chunks) > 1
    assert any([expected_content in chunk for chunk in chunks])
