from pathlib import Path

import pytest

from src.pdf import read


@pytest.mark.parametrize(
    "file,expected_title",
    [
        (
            "pdfs/icra_2020.pdf",
            "Supervisory control of robot swarms using public events",
        ),
        (
            "pdfs/openswarm_2016.pdf",
            "OpenSwarm: an event-driven embedded operating system for miniature robots",
        ),
        ("pdfs/sct_2016.pdf", "Supervisory control theory applied to swarm robotics"),
        (
            "pdfs/swarmcom_2020.pdf",
            "SwarmCom: an infra-red-based mobile ad-hoc network "
            "for severely constrained robots",
        ),
        (
            "pdfs/Trenkwalder_PhD.pdf",
            "Classification and Management of Computational Resources of "
            "Robotic Swarms and the Overcoming of their Constraints",
        ),
    ],
)
def test_pdfs_title_should_be_correct(file, expected_title):
    file_path = Path(__file__).resolve().parent / file

    data = read(str(file_path))

    assert data["title"] == expected_title


@pytest.fixture(scope="module")
def loaded_sct_paper():
    file_path = Path(__file__).resolve().parent / "pdfs/sct_2016.pdf"

    data = read(str(file_path))
    yield data


@pytest.mark.parametrize(
    "expected_content",
    [
        "However, they usually do not guarantee that the implementation",
        "matches the speciﬁcation, because the system’s control code "
        "is typically generated manually.",
        "However, they usually do not guarantee that the implementation\n"
        "matches the speciﬁcation, because the system’s control code "
        "is typically generated manually.",
        "between different robotic platforms. "
        "These advantages are demonstrated in four case studies",
    ],
)
def test_pdfs_content_should_be_correct(loaded_sct_paper, expected_content):
    data = loaded_sct_paper
    assert expected_content in data["content"]
